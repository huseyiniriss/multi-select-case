import {useEffect} from 'react';

type RefType = {
	current: any;
};

type OnClickOutsideType = (event: MouseEvent) => void;

function useOutsideClick(ref: RefType, onClickOutside: OnClickOutsideType) {
	useEffect(() => {
		function handleClickOutside(event: MouseEvent) {
			const current = ref.current as Node;
			if (!current.contains(event.target as Node)) {
				onClickOutside(event);
			}
		}

		// Bind the event listener
		document.addEventListener('mousedown', handleClickOutside);
		return () => {
			document.removeEventListener('mousedown', handleClickOutside);
		};
	}, [ref]);
}

export default useOutsideClick;
