import Select from '@/components/Select/Select.tsx';
import {useDeferredValue, useEffect, useState} from 'react';
import CharacterCard, {type CharacterType} from '@/components/CharacterCard.tsx';
import axios from 'axios';

type CharacterResponseType = {
	results: Array<{
		id: number;
		name: string;
		image: string;
		episode: string[];
	}>;
	error: string;
};

function App() {
	const [query, setQuery] = useState('');
	const [characters, setCharacters] = useState<CharacterType[]>([]);
	const [isLoading, setIsLoading] = useState<boolean>(false);
	const [err, setErr] = useState<string>('');
	const deferredQuery = useDeferredValue(query);

	const fetchCharacters = () => {
		if (!deferredQuery.length) {
			setCharacters([]);
			setErr('');
			setIsLoading(false);
			return;
		}

		setErr('');
		setIsLoading(true);
		axios.get<CharacterResponseType>('https://rickandmortyapi.com/api/character', {params: {name: deferredQuery}})
			.then(res => {
				if (res.data.error) {
					setErr(res.data.error);
				} else {
					const characters: CharacterType[] = res.data.results.map(character => ({
						value: character.id,
						label: character.name,
						image: character.image,
						episodes: character.episode.length,
						highlight: deferredQuery,
					}));
					setCharacters(characters);
				}
			})
			.catch(e => {
				const err = e.response.data.error as string;
				setErr(err);
				setCharacters([]);
			}).finally(() => {
				setIsLoading(false);
			});
	};

	useEffect(() => {
		fetchCharacters();
	}, [deferredQuery]);

	return (
		<main>
			<Select
				items={characters}
				placeholder='Search Rick and Morty Characters'
				onSearch={setQuery}
				itemRenderer={item => (
					<CharacterCard {...item as CharacterType} />
				)}
				isLoading={isLoading}
			/>
			{err && <p className='error'>Error : {err}</p>}
		</main>
	);
}

export default App;
