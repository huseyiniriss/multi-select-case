import {type ItemPropsType} from '@/components/Select/Selet.type.ts';
import {type FC} from 'react';

export type CharacterType = {
	image: string;
	highlight: string;
	episodes: number;
} & ItemPropsType;

const CharacterCard: FC<CharacterType> = props => {
	const label = new RegExp(props.highlight, 'ig');
	const characterLabel = props.label.replace(
		label,
		`<b>${props.highlight}</b>`,
	);
	return (
		<div className='character-card'>
			<img
				className='character-card__image'
				src={props.image}
				alt={props.label}
			/>
			<div className='character-card__info'>
				<span dangerouslySetInnerHTML={{__html: characterLabel}} />
				<span>{props.episodes} Episodes</span>
			</div>
		</div>
	);
};

export default CharacterCard;
