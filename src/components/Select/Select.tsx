import {
	type ChangeEvent,
	type FC, type KeyboardEventHandler, useEffect, useRef, useState,
} from 'react';
import styles from './Select.module.scss';
import {IoCaretDownOutline, IoCloseSharp} from 'react-icons/io5';
import {
	type ItemPropsType,
	type SelectPropsType,
} from '@/components/Select/Selet.type.ts';
import classNames from 'classnames';
import SelectedItem from '@/components/Select/SelectedItem.tsx';
import useOutsideClick from '@/hooks/useOutsideClick.ts';

const Select: FC<SelectPropsType> = props => {
	const [items, setItems] = useState<ItemPropsType[]>(props.items);
	const [selected, setSelected] = useState<ItemPropsType[]>([]);
	const [isOpen, setIsOpen] = useState<boolean>();
	const [selectedIndex, setSelectedIndex] = useState<number>(-1);
	const inputRef = useRef<HTMLInputElement>(null);
	const containerRef = useRef<HTMLDivElement>(null);
	const optionsRef = useRef<HTMLDivElement>(null);

	useOutsideClick(containerRef, () => {
		setIsOpen(false);
	});

	useEffect(() => {
		setItems(props.items);
	}, [props.items.length]);

	useEffect(() => {
		setSelected(props.value ?? []);
	}, [props.value?.length]);
	const onClickDelete = (item: ItemPropsType) => {
		setSelected(selected.filter(i => i.value !== item.value));
	};

	const onKeyDownInput: KeyboardEventHandler<HTMLInputElement> = e => {
		const input = e.currentTarget as HTMLInputElement;
		if (isOpen) {
			if (e.key === 'ArrowDown') {
				setSelectedIndex(prevState => {
					if ([-1, items.length - 1].includes(prevState)) {
						return 0;
					}

					return prevState + 1;
				});
			} else if (e.key === 'ArrowUp') {
				setSelectedIndex(prevState => {
					if ([-1, 0].includes(prevState)) {
						return props.items.length - 1;
					}

					return prevState - 1;
				});
			} else if (e.key === 'Enter') {
				if (selectedIndex > -1) {
					onClickSelect(items[selectedIndex]);
				}
			}

			if (optionsRef.current) {
				const item = optionsRef.current.children[selectedIndex];
				if (item) {
					item.scrollIntoView({block: 'center'});
				}
			}
		}

		if (e.key === 'Backspace' && selected.length > 0 && !input.value.length) {
			setSelected(selected.slice(0, -1));
		}
	};

	const onClickContainer = () => {
		inputRef.current?.focus();
		setSelectedIndex(-1);
		setIsOpen(true);
	};

	const onClickSelect = (item: ItemPropsType) => {
		if (selected.some(i => i.value === item.value)) {
			setSelected(selected.filter(i => i.value !== item.value));
		} else {
			setSelected([...selected, item]);
		}
	};

	const onChangeInput = (e: ChangeEvent) => {
		const input = e.currentTarget as HTMLInputElement;
		if (props.onSearch) {
			props.onSearch(input.value);
		}
	};

	const onClickClear = () => {
		setSelected([]);
	};

	const showOptions = isOpen && inputRef.current && inputRef.current?.value.length > 0 && !props.isLoading;

	return (
		<div className={styles['app-select-container']} onClick={onClickContainer} ref={containerRef}>
			<div className={styles['app-select']} aria-hidden>
				<div className={styles['app-select__selected']}>
					{selected.map(item => (
						<SelectedItem
							key={item.value}
							{...item}
							onClickDelete={onClickDelete}
						/>
					))}
					<input
						className={styles['app-select__selected__input']}
						type='text'
						onKeyDown={onKeyDownInput}
						ref={inputRef}
						placeholder={props.placeholder}
						onChange={onChangeInput}
						onFocus={() => {
							setIsOpen(true);
						}}
					/>
				</div>
				{selected.length ? (
					<IoCloseSharp size={22} color='#475569' onClick={onClickClear}/>
				) : (
					<IoCaretDownOutline color='#475569'/>
				)}
			</div>
			<div
				ref={optionsRef}
				className={classNames(styles['app-select__options'], {
					[styles['app-select__options-active']]: showOptions,
				})}
			>
				{props.isLoading && <div className={styles['app-select__options__empty']}>Loading...</div>}
				{!props.items.length && !props.isLoading && <div className={styles['app-select__options__empty']}>No results</div>}
				{props.items.map((item, index) => (
					<div
						key={item.value}
						className={classNames(styles['app-select__options__item'], {
							[styles['app-select__options__item-active']]:
                            selectedIndex === index,
						})}
						aria-hidden
						onClick={() => {
							onClickSelect(item);
						}}
					>
						<input
							type='checkbox'
							checked={selected?.some(e => e.value === item.value)}
							onChange={() => {
								onClickSelect(item);
							}}
							value={item.value}
						/>
						{props.itemRenderer ? (
							<div>{props.itemRenderer(item)}</div>
						) : (
							<label>{item.label}</label>
						)}
					</div>
				))}
			</div>
		</div>
	);
};

export default Select;
