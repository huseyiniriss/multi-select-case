import {type ReactNode} from 'react';

export type ItemPropsType = {
	value: string | number;
	label: string;
	onClickDelete?: (value: ItemPropsType) => void;
};

export type SelectPropsType = {
	value?: ItemPropsType[];
	onSelect?: (item: ItemPropsType) => void;
	items: ItemPropsType[];
	placeholder?: string;
	onSearch?: (value: string) => void;
	itemRenderer?: (item: ItemPropsType) => ReactNode;
	isLoading?: boolean;
};
