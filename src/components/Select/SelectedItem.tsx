import {type FC} from 'react';
import {type ItemPropsType} from '@/components/Select/Selet.type.ts';
import styles from '@/components/Select/Select.module.scss';
import {IoCloseSharp} from 'react-icons/io5';

const SelectedItem: FC<ItemPropsType> = props => (
	<div className={styles['app-select__selected__item']}>
		<span>{props.label}</span>
		<button className={styles['app-select__selected__item__remove']}
			onClick={() => {
				if (props.onClickDelete) {
					props.onClickDelete(props);
				}
			}}>
			<IoCloseSharp size={16} color='#fff'/>
		</button>
	</div>
);

export default SelectedItem;
