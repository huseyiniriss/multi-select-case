# This project is multi select dropdown component
* This project includes a multi select dropdown component that is created with React and TypeScript.

## Installation
```bash
npm install
```
 
## Usage
```bash
npm dev
```

## Project Structure
```bash
public
src
├── assets
│   └── styles
│       ├── _charecters.scss
│       ├── _reset.scss
│       ├── _variables.scss
│       └── _variables.scss
├── components
│   ├── Select
│   │   ├── Select.module.scss
│   │   ├── Select.tsx
│   │   ├── SelectItem.tsx
│   │   └── Select.type.tsx
│   └── CharacterCard.tsx
├── hooks
│   └── useOutsideClick.ts
├── App.tsx
├── main.tsx
└── vite-env.d.ts
.eslintrc.cjs
.gitignore
index.html
package.json
README.md
tsconfig.json
vite.config.ts
```

## Info
- This project is created with Vite.js
- This project is using TypeScript
- This project is using SCSS
- This project is using ESLint
- This project is using Prettier

## Test URL
- [https://multi-select-case-mu.vercel.app/](https://multi-select-case-mu.vercel.app/)
